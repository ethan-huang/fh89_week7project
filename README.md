## IDS721Project 7

- This project demonstrates the process of working with a vector database using Rust. And I implemented code for data ingestion and data query after setting up a database.

# Build Process

- `cargo new <project>` to start a new project
- add dependencies to the Cargo.toml file:
  qdrant-client = "1.8.0"
  tokio = { version = "1.36.0", features = ["rt-multi-thread"] }
  serde_json = "1.0.114"
  tonic = "0.11.0"
  anyhow = "1.0.81"
- add code into main.rs for data ingestion and data query

# Database set up

- Use the docker to the Qdrant database, `docker run -p 6333:6333 -p 6334:6334 \ -e QDRANT__SERVICE__GRPC_PORT="6334" \qdrant/qdrant`
- use cargo run to the run the project

# Result

- ![Qdrant database](/images/database.png)
- Generate 10 data points containing four floating-point values calculated based on the loop index, and constructs a JSON object (payload) using the json! macro from the serde_json crate. The JSON object contains a name, NetId, and metadata fields, with values derived from the loop index![Generate 10 data points containing four floating-point values calculated based on the loop index, and constructs a JSON object (payload) using the json! macro from the serde_json crate. The JSON object contains a name, NetId, and metadata fields, with values derived from the loop index](/images/generate.png)

- implement search function: It constructs a SearchPoints struct instance, providing parameters for the search operation.![implement search function: It constructs a SearchPoints struct instance, providing parameters for the search operation.](/images/search.png)

- dispalyed the data![dispalyed the data](/images/result.png)
